const fs = require('fs');
const { makeExecutableSchema } = require('graphql-tools');
const { graphql } = require('graphql');
const HTTP_CODE = require('http-status-codes');
const resolvers = require('../src/resolvers');
const mockMovieService = require('./mocks/mockMovieService');

const { dropDb } = require('./db');
const { studentTest, studentMissPassword, mentorTest } = require('./constants');
const { COLLECTIONS_NAME } = require('../src/constants');

const typeDefs = fs.readFileSync('./src/schemas/auth.graphql', 'utf8');
const schema = makeExecutableSchema({ typeDefs, resolvers });

const allMoviesTestCase = {
  mutation: `
      mutation {
        studentRegistration(
          email: "shohan.rahman@test.com"
          password: "123"
          firstName: "Shohan"
          lastName: "Rahman"
          city: "Paris"
          phone: "0659"
          ) {
            token
            status {
              code
              message
              success
            }
          }
      }
    `,
  variables: {},

  // Injecting the mock movie server with canned responses
  context: { movieService: mockMovieService },

  // Expected result
  expected: {
    data: { studentRegistration: { code: HTTP_CODE.CREATED, success: true, message: 'OK' } },
  },
};

describe('Auth Student', () => {
  beforeEach(async () => {
    await dropDb(COLLECTIONS_NAME.STUDENT_AUTH);
  });

  test('mutation: studentAuth -> 201', async () => {
    const result = await graphql(schema, studentTest, null, null, null);
    return expect(result.data.studentRegistration.status.code).toEqual(HTTP_CODE.CREATED);
  });

  test('mutation: studentRegistration -> 400', async () => {
    const result = await graphql(schema, studentMissPassword, null, null, null);
    return expect(result.errors.length).toEqual(1);
  });

  test('mutation: studentRegistration -> 409', async () => {
    await graphql(schema, studentTest, null, null, null);
    const resultFailed = await graphql(schema, studentTest, null, null, null);
    return expect(resultFailed.data.studentRegistration.status.code).toEqual(HTTP_CODE.CONFLICT);
  });
});

// describe('Schema', () => {
//   // Array of case typesresolvers
//   const cases = [allMoviresolvers
//   const typeDefs = fs.readFileSync('./src/schemas/auth.graphql', 'utf8');
//   const schema = makeExecutableSchema({ typeDefs, resolvers });

//   cases.forEach((obj) => {
//     const {
//       id, mutation, variables, context, expected,
//     } = obj;
//     test(`mutation: ${id}`, async () => {
//       const result = await graphql(schema, mutation, null, {}, {});
//       console.log('result:', result.data.studentRegistration.status);
//       return expect(result.data.studentRegistration.status.code).toEqual(HTTP_CODE.CREATED);
//     });
//   });
// });
