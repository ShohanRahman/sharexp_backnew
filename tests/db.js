const { getDBConnection, getDB } = require('../src/db');

const dropDb = async (collection) => {
  const criteria = {}; // a remplir avec les compte test que l'on veut suppr
  const dbConnection = await getDBConnection();
  const db = await getDB(dbConnection);
  await db.collection(collection).find(criteria);
  await db.dropDatabase();
  await dbConnection.close();
};

module.exports.dropDb = dropDb;
