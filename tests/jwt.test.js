const jwt = require('../src/jwtUtils.js');

describe('Test JWT', () => {
  test('Generate token -> SUCCESS', async () => {
    const token = jwt.generateToken(12);
    return expect(token).not.toBe(false);
  });

  test('Generate token -> FAILED', async () => {
    const token = jwt.generateToken(null);
    return expect(token).toBeFalsy();
  });

  test('Verify token -> SUCCESS', async () => {
    const token = jwt.generateToken(12);
    const anwser = jwt.verifyToken(token);
    return expect(anwser.userId).toEqual(12);
  });

  test('Verify token -> FAILED', async () => {
    const anwser = jwt.verifyToken(null);
    return expect(anwser).toBeFalsy();
  });
});
