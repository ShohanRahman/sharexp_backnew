const studentTest = `
mutation {
  studentRegistration(
    email: "test@test.com"
    password: "123ade2E"
    firstName: "Shohan"
    lastName: "Rahman"
    city: "Paris"
    phone: "0659"
    ) {
      token
      status {
        code
        message
        success
      }
    }
}
`;

const studentMissPassword = `
mutation {
  studentRegistration(
    email: "test@test.com"
    firstName: "Shohan"
    lastName: "Rahman"
    city: "Paris"
    phone: "0659"
    ) {
      token
      status {
        code
        message
        success
      }
    }
}
`;

const mentorTest = `
mutation {
  mentorFields(email:"yaniss.pheron@epitech.eu",  firstName: "read",
      lastName: "BG",
      job: "Alchimist",
      company: "TEKforGOOD",
      phone: "123456789",
      city: "Paris")
  {
    status{
      code
      message
    }
  }
}
`;

const mentorMissEmail = `
mutation {
  mentorFields(firstName: "read",
      lastName: "BG",
      job: "Alchimist",
      company: "TEKforGOOD",
      phone: "123456789",
      city: "Paris")
  {
    status{
      code
      message
    }
  }
}
`;

module.exports = {
  studentTest,
  studentMissPassword,
  mentorTest,
  mentorMissEmail,
};
