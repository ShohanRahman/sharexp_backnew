const fs = require('fs');
const { makeExecutableSchema } = require('graphql-tools');
const { graphql } = require('graphql');
const HTTP_CODE = require('http-status-codes');
const resolvers = require('../src/resolvers');

const { dropDb } = require('./db');
const { mentorTest, mentorMissEmail } = require('./constants');
const { COLLECTIONS_NAME } = require('../src/constants');

const typeDefs = fs.readFileSync('./src/schemas/auth.graphql', 'utf8');
const schema = makeExecutableSchema({ typeDefs, resolvers });

describe('Auth Mentor', () => {
  beforeEach(() => {
    dropDb(COLLECTIONS_NAME.MENTOR_FIELDS);
  });

  test('mutation: mentorField -> 201', async () => {
    const result = await graphql(schema, mentorTest, null, null, null);
    return expect(result.data.mentorFields.status.code).toEqual(HTTP_CODE.CREATED);
  });

  test('mutation: mentorField -> 400', async () => {
    const result = await graphql(schema, mentorMissEmail, null, null, null);
    return expect(result.errors.length).toEqual(1);
  });

  test('mutation: mentorField -> 409', async () => {
    await graphql(schema, mentorTest, null, null, null);
    const resultFailed = await graphql(schema, mentorTest, null, null, null);
    return expect(resultFailed.data.mentorFields.status.code).toEqual(HTTP_CODE.CONFLICT);
  });
});
