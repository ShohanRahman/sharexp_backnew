const joi = require('joi');
const HTTP_CODE = require('http-status-codes');
const bcrypt = require('bcrypt');

const { getDBConnection, getDB } = require('./db');
const { mentorFieldsSchemas } = require('./schemas/mentorSchemas');
const { studentAuthMutationSchemas, studentAuthQuerySchemas } = require('./schemas/studentSchemas');
const { COLLECTIONS_NAME } = require('./constants');
const { generateToken } = require('./jwtUtils');

const resolvers = {
  Query: {
    async studentAuth(parent, args) {
      try {
        const { error } = joi.validate(args, studentAuthQuerySchemas, { abortEarly: false });

        if (error) {
          return { status: { code: HTTP_CODE.BAD_REQUEST, message: String(error), succes: false } };
        }

        const dbConnection = await getDBConnection();
        const db = await getDB(dbConnection);
        const criteria = {
          email: args.email,
        };

        const result = await db.collection(COLLECTIONS_NAME.STUDENT_AUTH).findOne(criteria);
        if (result) {
          const match = await bcrypt.compare(args.password, result.password);
          if (match) {
            dbConnection.close();
            return {
              token: generateToken(result._id),
              status: {
                code: HTTP_CODE.OK,
                message: 'Connected',
                success: true,
              },
            };
          }
          dbConnection.close();
          return {
            status: {
              code: HTTP_CODE.UNAUTHORIZED,
              message: 'Authentification failed : Wrong password',
              success: false,
            },
          };
        }
        dbConnection.close();
        return {
          status: { code: HTTP_CODE.NOT_FOUND, message: 'Email not found.', success: false },
        };
      } catch (err) {
        return { code: HTTP_CODE.INTERNAL_SERVER_ERROR, message: err, success: false };
      }
    },
  },
  Mutation: {
    async studentAuth(parent, args) {
      try {
        const { error } = joi.validate(args, studentAuthQuerySchemas, { abortEarly: false });

        if (error) {
          return { status: { code: HTTP_CODE.BAD_REQUEST, message: String(error), succes: false } };
        }

        const dbConnection = await getDBConnection();
        const db = await getDB(dbConnection);
        const criteria = {
          email: args.email,
        };

        const result = await db.collection(COLLECTIONS_NAME.STUDENT_AUTH).findOne(criteria);
        if (result) {
          const match = await bcrypt.compare(args.password, result.password);
          if (match) {
            dbConnection.close();
            return {
              token: generateToken(result._id),
              status: {
                code: HTTP_CODE.OK,
                message: 'Connected',
                success: true,
              },
            };
          }
          dbConnection.close();
          return {
            status: {
              code: HTTP_CODE.UNAUTHORIZED,
              message: 'Authentification failed : Wrong password',
              success: false,
            },
          };
        }
        dbConnection.close();
        return {
          status: { code: HTTP_CODE.NOT_FOUND, message: 'Email not found.', success: false },
        };
      } catch (err) {
        return { code: HTTP_CODE.INTERNAL_SERVER_ERROR, message: err, success: false };
      }
    },
    async studentRegistration(parent, args) {
      try {
        const { error } = joi.validate(args, studentAuthMutationSchemas, { abortEarly: false });

        if (error) {
          return { status: { code: HTTP_CODE.BAD_REQUEST, message: String(error), succes: false } };
        }

        const dbConnection = await getDBConnection();
        const db = await getDB(dbConnection);
        const criteria = {
          email: args.email,
        };

        const existInDb = await db.collection(COLLECTIONS_NAME.STUDENT_AUTH).findOne(criteria);
        if (existInDb) {
          dbConnection.close();
          return {
            status: {
              code: HTTP_CODE.CONFLICT,
              message: 'Registration failed : Already an account with this email',
              success: false,
            },
          };
        }

        let id;
        // eslint-disable-next-line no-unused-vars
        await bcrypt.hash(args.password, 5).then(async (res, err) => {
          const passwordCrypt = res;

          const student = {
            email: args.email,
            password: passwordCrypt,
            createdAt: new Date(),
            updatedAt: new Date(),
          };

          const result = await db
            .collection(COLLECTIONS_NAME.STUDENT_AUTH)
            .insertOne(student)
            .then(response => response.insertedId);
          id = result;
        });
        dbConnection.close();
        return {
          token: generateToken(id),
          status: {
            code: HTTP_CODE.CREATED,
            message: 'Student profile created with success',
            success: true,
          },
        };
      } catch (err) {
        return { code: HTTP_CODE.INTERNAL_SERVER_ERROR, message: err, success: false };
      }
    },
    async mentorFields(parent, args) {
      try {
        const { error } = joi.validate(args, mentorFieldsSchemas, { abortEarly: false });

        if (error) {
          return {
            code: HTTP_CODE.BAD_REQUEST,
            message: String(error),
            succes: false,
          };
        }

        const dbConnection = await getDBConnection();
        const db = await getDB(dbConnection);

        let criteria = {
          email: args.email,
        };
        const existInDb = await db.collection(COLLECTIONS_NAME.MENTOR_FIELDS).findOne(criteria);
        if (existInDb) {
          dbConnection.close();
          return {
            status: {
              code: HTTP_CODE.CONFLICT,
              message: 'Authentification failed : Already an account with this email.',
              success: false,
            },
          };
        }

        // eslint-disable-next-line no-param-reassign
        delete args.id;
        criteria = {
          ...args,
        };
        const result = await db
          .collection(COLLECTIONS_NAME.MENTOR_FIELDS)
          .insertOne(criteria)
          .then(response => response.insertedId);
        dbConnection.close();
        return {
          token: generateToken(result),
          status: {
            code: HTTP_CODE.CREATED,
            message: 'Informative fields are created',
            success: true,
          },
        };
      } catch (err) {
        return { code: HTTP_CODE.INTERNAL_SERVER_ERROR, message: err, success: false };
      }
    },
  },
};

module.exports = resolvers;
