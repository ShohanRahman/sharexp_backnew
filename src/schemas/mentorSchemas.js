const joi = require('joi');

const mentorFieldsSchemas = joi.object().keys({
  firstName: joi.string().required(),
  lastName: joi.string().required(),
  email: joi.string().required(),
  city: joi.string().required(),
  job: joi.string().required(),
  company: joi.string().required(),
  phone: joi.string().required(),
});

module.exports = {
  mentorFieldsSchemas,
};
