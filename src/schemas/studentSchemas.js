const joi = require('joi');

const studentAuthQuerySchemas = joi.object().keys({
  email: joi.string().required(),
  password: joi.string().required(),
});

const studentAuthMutationSchemas = joi.object().keys({
  email: joi
    .string()
    .email()
    .required(),
  password: joi
    .string()
    .regex(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/)
    .min(8)
    .max(16)
    .required(),
  firstName: joi.string().required(),
  lastName: joi.string().required(),
  city: joi.string().required(),
  phone: joi.string().required(),
});

module.exports = {
  studentAuthMutationSchemas,
  studentAuthQuerySchemas,
};
