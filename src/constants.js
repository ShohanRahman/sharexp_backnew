const COLLECTIONS_NAME = Object.freeze({
  MENTOR_FIELDS: 'mentorFields',
  STUDENT_AUTH: 'studentAuth',
});

module.exports = {
  COLLECTIONS_NAME,
};
