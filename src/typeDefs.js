const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Status {
    code: Int!
    message: String
    success: Boolean
  }

  type Auth {
    status: Status
    token: String
  }

  type Query {
    studentAuth(email: String!, password: String!): Auth
  }

  type Mutation {
    studentAuth(email: String!, password: String!): Auth
    studentRegistration(
      email: String!
      password: String!
      firstName: String!
      lastName: String!
      city: String!
      phone: String!
    ): Auth
    mentorFields(
      email: String!
      firstName: String!
      lastName: String!
      job: String!
      company: String!
      phone: String!
      city: String!
    ): Auth
  }
`;

module.exports = typeDefs;
