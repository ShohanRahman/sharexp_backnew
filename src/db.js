const { MongoClient } = require('mongodb');
const { mongoAuth } = require('../config');

const dbConnection = Object.freeze(
  MongoClient.connect(
    `mongodb+srv://${mongoAuth.USER}:${mongoAuth.PSSWD}@graphql-lqliq.mongodb.net`,
    { useNewUrlParser: true },
  ),
);

const getDBConnection = async () => {
  const client = await MongoClient.connect(
    `mongodb+srv://${mongoAuth.USER}:${mongoAuth.PSSWD}@graphql-lqliq.mongodb.net`,
    { useNewUrlParser: true },
  );
  return client;
};

const getDB = async dbConnect => dbConnect.db('Users');

module.exports = {
  getDBConnection,
  dbConnection,
  getDB,
};
