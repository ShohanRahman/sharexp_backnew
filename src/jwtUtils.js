const jwt = require('jsonwebtoken');
const JWT_SECRET_KEY = require('../config');

module.exports = {
  /**
   * @userId the id's user to generate token
   */
  generateToken(userId) {
    if (userId === false || userId === undefined || userId === '' || userId === null) {
      return false;
    }

    return jwt.sign(
      {
        userId,
      },
      JWT_SECRET_KEY.jwt,
      {
        expiresIn: '48h',
      },
    );
  },
  /**
   * @token the token generated
   */
  verifyToken(token) {
    return jwt.verify(token, JWT_SECRET_KEY.jwt, (err, decoded) => {
      if (err) return false;

      return decoded;
    });
  },
};
