# Workflow guide

## Création de branche

Avant de commencer une nouvelle tâche, on doit d’abords se placer sur stage : 

		   
		 git  checkout stage 
		   
Pour créer sa nouvelle branche avec la commande suivante : 

		 git branch [nom] 

>***Note**:   (Si ce n'est pas fait il faut préciser le nom de la branche sur la tâche du [Trello](https://trello.com/b/lcN0LyWU/sharexp))*

Une fois la branche créée, on peut se positionner dessus avec la commande :

		git checkout [nom]

A partir de là on peut commencer à coder ! 
(Pas oublier de push régulièrement et de garder des messages de commit propres et détaillés)

## Merge

Si nécessaire il est possible de récupérer les changements sur stage avec :
		
		git merge stage

Une fois la tâche terminée, on se positionne sur stage  : 

		 git checkout stage &&
		    git merge [nom] && 
		    git push

En cas de conflits, il est préférable de les régler sur sa branche en récupérant au préalable les changements de stage depuis sa branche : 

		git checkout [nom] &&
		git merge stage && 
		git push

## Mise en production 
Une fois le sprint terminé et les nouvelles comme les anciennes fonctionnalités testées sur la branche stage, on peut se positionner sur master et merge depuis stage.

		git checkout master && 
		git merge stage &&
		git push

> Les serveurs de production devront se positionner sur **master** et obtenir les nouvelles versions avec un `script de déploiement automatique` effectuant un `pull origin master` et un restart de l'hôte (npm).